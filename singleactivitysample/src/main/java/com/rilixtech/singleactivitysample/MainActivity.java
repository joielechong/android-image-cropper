package com.rilixtech.singleactivitysample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;
import com.theartofdev.edmodo.cropper.ActivityResult;
import com.theartofdev.edmodo.cropper.CropImage;

public class MainActivity extends AppCompatActivity {

  ImageView imageView;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    imageView = findViewById(R.id.ImageView);

    imageView.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View view) {
        showCropImage();
      }
    });
  }

  private void showCropImage() {
    CropImage.with(this).setListener(new CropImage.ResultListener() {
      @Override public void onCropImageComplete(ActivityResult result) {
        Toast.makeText(MainActivity.this, "Result = " + result, Toast.LENGTH_SHORT).show();
        imageView.setImageURI(result.getUri());
      }
    }).start();
  }
}
