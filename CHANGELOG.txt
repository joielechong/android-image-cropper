*2.7.0*
- Update gradle wrapper to 4.4
- Update support library to 27.1.1 and set is statically! (thx @androideveloper)
- Fix NPE in activity creation by tools (thx @unverbraucht)
- More translations (thx @gwharvey, @dlackty, @JairoGeek, @shaymargolis)

*2.6.0*
- Update to sdk v27
- Update to gradle v3

*2.5.1*
- Try solve manifest merger issue by adding `transitive` flag #405 (thx @j-garin)
- Use thread pool executors for async image loading and cropping operations to prevent app hang if default executor is busy (thx @ruifcardoso)
- Fix image rotation breaking min/max crop result restrictions #401
- Propagate all extra data set on start crop activity intent back in crop result intent #352
