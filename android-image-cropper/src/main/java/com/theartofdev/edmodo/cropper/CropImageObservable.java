package com.theartofdev.edmodo.cropper;

import java.util.Observable;

class CropImageObservable extends Observable {
  private static CropImageObservable instance;

  private CropImageObservable(){}

  static CropImageObservable getInstance() {
    if(instance == null) {
      synchronized (CropImageObservable.class) {
        if(instance == null) instance = new CropImageObservable();
      }
    }

    return instance;
  }

  void updateResult(ActivityResult result) {
    synchronized (this) {
      setChanged();
      notifyObservers(result);
    }
  }
}
